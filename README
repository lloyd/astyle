------------------------------------------------------------------------------------------

astyle.py - A Python program to check text against the ATLAS Style Guidelines

------------------------------------------------------------------------------------------

Contact: s.l.lloyd@qmul.ac.uk

Some additional grammar rules taken from Neil Spring's style-check see:
http://www.cs.umd.edu/~nspring/software/style-check-readme.html

Copyright 2017-24 Steve Lloyd - Queen Mary University of London
MIT License

------------------------------------------------------------------------------------------

Files:

astyle.py        - The main program
astyle_config.py - Configuration options and the ATLAS Style rules definitions
astyle_cfg.py    - Example user configuration file
astyle_rules.txt - Miscellaneous grammar and syntax rules
LICENSE          - MIT License file	
README           - This file

------------------------------------------------------------------------------------------

usage: astyle.py [-h] [-c CONFIG] [-r RULES] [-o OUTPUT]
                 [-t THRESHOLD] [-v] [-l] [-a | -b]
                 input

positional arguments:
  input                 input LaTeX file

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        configuration file
  -r RULES, --rules RULES
                        custom rules file
  -o OUTPUT, --output OUTPUT
                        output file
  -t THRESHOLD, --threshold THRESHOLD
                        threshold - 'high' (less warnings), 'medium', 'low'
                        (more warnings)
  -v, --verbose         verbose output - prints the rule criteria
  -l, --orderbylines    order output by lines (rather than by tests)
  -a, --american        American English
  -b, --british         British English (default)

------------------------------------------------------------------------------------------

The program reads the input LaTeX file and any subsequent files refered to by \input{...} and attempts to check the text against various ATLAS Style Guidelines and miscellaneous grammar and syntax rules. The ATLAS Style rules are defined in astyle_config.py and can be modified/extended by copying and modifying the file astyle_cfg.py and including it via the -c option.

The file astyle_rules.txt contains other grammar and syntax rules - many taken from Neil Spring's style-check files. Custom rules can be written in another file and included via the -r option. The syntax of this file is a number of lines each of which has the format:

search term     %rule suggestions

where a regular expression is constructed for each rule as follows:
phrase      - '\b(search term)\b' - case insensitive
spelling    - '\b(search term)\b' - case insensitive
capitalize  - '\b(search term)\b' - case sensitive
syntax      - '(search term)'     - case sensitive
hyphenate   - '\b(search term)\b' - case insensitive
english     - special case whereby the search term is the British spelling and the suggestion the American

The amount of whitespace is not important and lines stating with # are ignored.

example:

# Custom rules file
#
Something I don't like	% phrase replace with something else
thsee                   % spelling these
gaussian                % capitalize Gaussian
[ ]\\footnote           % syntax no space before footnote.  
Breit-Wigner            % hyphenate Breit--Wigner
center(s|ing)?          % english centre(s|ing)?

The output provides the file name, line number, the text found, its context and a suggestion (if present):

File: samples.tex line 40 -
Found "in the range" Context: "...period and are in the range 20--26~\GeV\  for..."  Suggest: in the range of

